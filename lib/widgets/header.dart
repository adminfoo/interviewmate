import 'package:flutter/material.dart';

header(context, {bool isAppTitle = false, String titleText = "Interview Mate"}) {
  return AppBar(
    title: Text(
      isAppTitle ? "Interview Mate" : titleText,
      style: TextStyle(
          color: Colors.white,
          fontFamily: isAppTitle ? "Signatra" : "",
          fontSize: isAppTitle ? 50.0 : 22.0),
    ),
    centerTitle: true,
    backgroundColor: Theme.of(context).accentColor,
  );
}
