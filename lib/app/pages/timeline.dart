import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:interviewmate/widgets/header.dart';
import 'package:interviewmate/widgets/progress.dart';

final userRef = FirebaseFirestore.instance.collection(('users'));

class Timeline extends StatefulWidget {
  @override
  _TimelineState createState() => _TimelineState();
}

class _TimelineState extends State<Timeline> {
  @override
  Widget build(context) {
    return Scaffold(
      appBar: header(context, isAppTitle: true),
      body: linearProgress(),
    );
  }

  @override
  void initState() {
    //getUsers();
    //getUserById();
    super.initState();
  }

  getUsers() async {
    final snapshot = await userRef.where("isAdmin", isEqualTo: true).get();
    snapshot.docs.forEach((doc) {
      print(doc.data);
    });
  }

  void getUserById() {
    userRef.doc("bljSskYES2uCq2y0Edsu").get().then((documentSnapshot) {
      print(documentSnapshot.data);
    });
  }
}
