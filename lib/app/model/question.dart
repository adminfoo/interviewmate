import 'package:equatable/equatable.dart';

class Question extends Equatable {
  const Question(
      {required this.id,
      required this.questionText,
      required this.lastUpdated,
      List<String>? jobs,
      List<String>? topics,
      int? timedSeconds})
      : this.jobs = jobs ?? const <String>[],
        this.topics = topics ?? const <String>[],
        this.timedSeconds = timedSeconds ?? 0;

  final String id;
  final String questionText;
  final List<String> jobs;
  final List<String> topics;
  final int timedSeconds;
  final DateTime lastUpdated;

  @override
  List<Object> get props =>
      [id, questionText, jobs, topics, timedSeconds, lastUpdated];

  @override
  bool get stringify => true;

  String get timedDurationToString => "";

  factory Question.fromMap(Map<dynamic, dynamic>? value, String id) {
    if (value == null) {
      throw StateError('missing data for entryId: $id');
    }
    final lastUpdatedInMillis = value['lastUpdated'] as int;
    return Question(
      id: id,
      questionText: value['questionText'] as String,
      lastUpdated: DateTime.fromMillisecondsSinceEpoch(lastUpdatedInMillis),
      jobs: value['jobs'] as List<String>? ?? const <String>[],
      topics: value['topics'] as List<String>? ?? const <String>[],
      timedSeconds: value['timedSeconds'] as int,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'questionText': questionText,
      'lastUpdated': lastUpdated.millisecondsSinceEpoch,
      'jobs': jobs,
      'topics': topics,
      'timedSeconds': timedSeconds
    };
  }
}
