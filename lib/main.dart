import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:interviewmate/app/account/account_page.dart';
import 'package:interviewmate/app/home_page.dart';
import 'package:interviewmate/app/pages/activity_feed.dart';
import 'package:interviewmate/app/pages/home.dart';
import 'package:interviewmate/routing/app_router.dart';
import 'package:interviewmate/services/shared_preferences_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app/auth_widget.dart';
import 'app/onboarding/onboarding_page.dart';
import 'app/onboarding/onboarding_view_model.dart';
import 'app/signin/sign_in_page.dart';
import 'app/top_level_providers.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final sharedPreferences = await SharedPreferences.getInstance();
  runApp(ProviderScope(
    overrides: [
      sharedPreferencesServiceProvider.overrideWithValue(
        SharedPreferencesService(sharedPreferences),
      ),
    ],
    child: MyApp(),
  ));
}

class MyApp extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final firebaseAuth = ref.watch(firebaseAuthProvider);
    return MaterialApp(
      title: 'InterviewMate',
      theme: ThemeData(
          primarySwatch: Colors.deepPurple, accentColor: Colors.teal),
      debugShowCheckedModeBanner: false,
      home: AuthWidget(
        nonSignedInBuilder: (_) => Consumer(
          builder: (context, ref, _) {
            final didCompleteOnboarding =
            ref.watch(onboardingViewModelProvider);
            return didCompleteOnboarding ? SignInPage() : OnboardingPage();
          },
        ),
        signedInBuilder: (_) => HomePage(),
      ),
      onGenerateRoute: (settings) => AppRouter.onGenerateRoute(settings, firebaseAuth),
    );
  }
}
